var indexSectionsWithContent =
{
  0: "_abcdfgilmnprstuv",
  1: "adtv",
  2: "adm",
  3: "abcdgiprstu",
  4: "_acdnptuv",
  5: "t",
  6: "bcfil",
  7: "abfnprv",
  8: "a"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Macros",
  8: "Pages"
};

